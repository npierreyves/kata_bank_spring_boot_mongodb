package fr.pyn.kata.bank.service;


import fr.pyn.kata.bank.domain.BankAccount;
import fr.pyn.kata.bank.domain.Operation;
import fr.pyn.kata.bank.domain.OperationType;
import fr.pyn.kata.bank.domain.dto.AccountDto;
import fr.pyn.kata.bank.repository.BankAccountRepository;
import fr.pyn.kata.bank.repository.OperationRepository;
import fr.pyn.kata.bank.utils.NoSuchAccountException;
import org.hamcrest.collection.IsEmptyCollection;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.TestPropertySource;

import java.time.Instant;
import java.util.*;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@SpringBootTest
@TestPropertySource(properties = "spring.autoconfigure.exclude=org.springframework.boot.autoconfigure.mongo.embedded.EmbeddedMongoAutoConfiguration")
public class BankAccountServiceTest {

    @MockBean
    private BankAccountRepository bankAccountRepository;

    @MockBean
    private OperationRepository operationRepository;

    @MockBean
    private AccountDtoMapper accountDtoMapper;

    @Autowired
    private BankAccountService bankAccountService;

    private List<Operation> operations;
    private BankAccount account;

    @BeforeEach
    public void setUp() {
        account = new BankAccount();
        account.setId(UUID.randomUUID().toString());
        account.setBalance(54321);
        account.setBankAccountNumber(42L);

        operations = new ArrayList<>();
        operations.add(new Operation(Instant.now(), OperationType.DEPOSIT, 12345, account.getId()));
    }

    @Test
    public void listAllOperations_should_throw_exception_for_no_such_account() throws Exception {
        when(bankAccountRepository.findByBankAccountNumber(ArgumentMatchers.anyLong())).thenReturn(Optional.empty());

        NoSuchAccountException expectedException = Assertions.assertThrows(NoSuchAccountException.class, () -> {
            bankAccountService.listAllOperations(666L);
        });

        assertThat(expectedException, is(notNullValue()));
        assertThat(expectedException.getMessage(), equalTo("No account matched provided bank account number : 666"));
    }


    @Test
    public void listAllOperations_should_successfully_return_all_account_operations() throws NoSuchAccountException {
        when(bankAccountRepository.findByBankAccountNumber(42L)).thenReturn(Optional.of(account));
        when(operationRepository.findByRefBankAccountId(account.getId())).thenReturn(operations);
        when(accountDtoMapper.mapEntityToDto(ArgumentMatchers.any(BankAccount.class), ArgumentMatchers.anyList())).thenCallRealMethod();

        List<Operation> operationList = bankAccountService.listAllOperations(42L);

        assertThat(operationList, is(notNullValue()));
        assertThat(operationList, hasSize(1));
    }

    @Test
    public void printStatement_should_throw_exception_for_no_such_account() throws NoSuchAccountException {
        when(bankAccountRepository.findByBankAccountNumber(anyLong())).thenReturn(Optional.empty());

        NoSuchAccountException expectedException = Assertions.assertThrows(NoSuchAccountException.class, () -> {
            bankAccountService.printStatement(666L);
        });

        assertThat(expectedException, is(notNullValue()));
        assertThat(expectedException.getMessage(), equalTo("No account matched provided bank account number : 666"));
    }

    @Test
    public void printStatement_should_successfully_return_current_account_balance() throws NoSuchAccountException {
        when(bankAccountRepository.findByBankAccountNumber(42L)).thenReturn(Optional.of(account));
        when(operationRepository.findByRefBankAccountId(account.getId())).thenReturn(operations);
        when(accountDtoMapper.mapEntityToDto(ArgumentMatchers.any(BankAccount.class), ArgumentMatchers.anyList())).thenCallRealMethod();

        AccountDto accountDto = bankAccountService.printStatement(42L);
        assertThat(accountDto.getBalance(), equalTo(account.getBalance()));
        assertThat(accountDto.getLatestOperations(), not(IsEmptyCollection.empty()));
        assertThat(accountDto.getLatestOperations(), hasSize(operations.size()));

        Operation operation = new Operation(Instant.now().minusSeconds(10000), OperationType.DEPOSIT, 12345, account.getId());
        operations.add(operation);
        when(bankAccountRepository.findByBankAccountNumber(anyLong())).thenReturn(Optional.of(account));
        when(operationRepository.findByRefBankAccountId(anyString())).thenReturn(operations);

        accountDto = bankAccountService.printStatement(42L);
        assertThat(accountDto.getLatestOperations(), hasSize(2));
        org.assertj.core.api.Assertions.assertThat(accountDto.getLatestOperations()).isSortedAccordingTo(Comparator.comparing(Operation::getDate).reversed());

    }


}

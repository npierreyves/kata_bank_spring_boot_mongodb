package fr.pyn.kata.bank.service;

import fr.pyn.kata.bank.domain.BankAccount;
import fr.pyn.kata.bank.domain.Operation;
import fr.pyn.kata.bank.domain.OperationType;
import fr.pyn.kata.bank.domain.dto.AccountDto;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.shadow.com.univocity.parsers.common.input.LineSeparatorDetector;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.lessThanOrEqualTo;

@SpringBootTest
public class AccountDtoMapperTest {

    private AccountDtoMapper dtoMapper;

    private BankAccount account;
    private List<Operation> operationList;

    @BeforeEach
    public void setUp() {
        dtoMapper = new AccountDtoMapper();
        operationList = new ArrayList<>();
        account = new BankAccount();
        account.setBankAccountNumber(42L);
        account.setBalance(54321);

        for (int i = 0; i < 15; i++) {
            Operation operation = new Operation(
                    Instant.now().minusSeconds(i),
                    (i % 2 == 0) ? OperationType.DEPOSIT : OperationType.WITHDRAWAL,
                    12345,
                    account.getId());

            operationList.add(operation);
        }
    }

    @Test
    public void mapEntityToDto_should_return_account_overview() {
        AccountDto accountDto = dtoMapper.mapEntityToDto(account, operationList);
        assertThat(accountDto.getBalance(), is(account.getBalance()));
        assertThat(accountDto.getLatestOperations(), hasSize(10));
        assertThat(accountDto.getLatestOperations().size(), lessThanOrEqualTo(operationList.size()));
        Assertions.assertThat(accountDto.getLatestOperations()).isSortedAccordingTo(Comparator.comparing(Operation::getDate).reversed());
    }

}


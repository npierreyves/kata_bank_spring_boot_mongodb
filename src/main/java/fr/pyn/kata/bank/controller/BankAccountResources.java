package fr.pyn.kata.bank.controller;


import fr.pyn.kata.bank.domain.Operation;
import fr.pyn.kata.bank.domain.dto.AccountDto;
import fr.pyn.kata.bank.domain.dto.OperationCommand;
import fr.pyn.kata.bank.service.BankAccountService;
import fr.pyn.kata.bank.service.OperationService;
import fr.pyn.kata.bank.utils.NoSuchAccountException;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/accounts/")
public class BankAccountResources {

    private final BankAccountService bankAccountService;
    private final OperationService operationService;

    public BankAccountResources(BankAccountService bankAccountService, OperationService operationService) {
        this.bankAccountService = bankAccountService;
        this.operationService = operationService;
    }

    @GetMapping("{bankAccountNumber}")
    public AccountDto printAccount(@PathVariable Long bankAccountNumber) throws NoSuchAccountException {
        return bankAccountService.printStatement(bankAccountNumber);
    }

    @GetMapping("{bankAccountNumber}/history")
    public List<Operation> getOperationsList(@PathVariable Long bankAccountNumber) throws NoSuchAccountException {
        return bankAccountService.listAllOperations(bankAccountNumber);
    }


    @PutMapping(value = "{bankAccountNumber}/deposit")
    public AccountDto deposit(@PathVariable Long bankAccountNumber,
                              @RequestBody OperationCommand operationCommand) throws NoSuchAccountException {
        return operationService.doDeposit(bankAccountNumber, operationCommand.getAmount());
    }

    @PutMapping(value = "{bankAccountNumber}/withdrawal")
    public AccountDto withdrawal(@PathVariable Long bankAccountNumber,
                                 @RequestBody OperationCommand operationCommand) throws NoSuchAccountException {
        return operationService.doWithdrawal(bankAccountNumber, operationCommand.getAmount());
    }
}

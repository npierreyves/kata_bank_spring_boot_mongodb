package fr.pyn.kata.bank.service;

import com.google.common.annotations.VisibleForTesting;
import fr.pyn.kata.bank.domain.BankAccount;
import fr.pyn.kata.bank.domain.Operation;
import fr.pyn.kata.bank.domain.OperationType;
import fr.pyn.kata.bank.domain.dto.AccountDto;
import fr.pyn.kata.bank.repository.BankAccountRepository;
import fr.pyn.kata.bank.repository.OperationRepository;
import fr.pyn.kata.bank.utils.NoSuchAccountException;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

@Service
public class OperationService {

    private final OperationRepository operationRepository;
    private final BankAccountRepository bankAccountRepository;
    private final AccountDtoMapper dtoMapper;

    public OperationService(OperationRepository operationRepository, BankAccountRepository bankAccountRepository, AccountDtoMapper dtoMapper) {
        this.operationRepository = operationRepository;
        this.bankAccountRepository = bankAccountRepository;
        this.dtoMapper = dtoMapper;
    }

    public AccountDto doWithdrawal(Long bankAccountNumber, long amount) throws NoSuchAccountException {
        Optional<BankAccount> optionalBankAccount = bankAccountRepository.findByBankAccountNumber(bankAccountNumber);
        if (!optionalBankAccount.isPresent()) {
            throw new NoSuchAccountException(bankAccountNumber);
        }
        BankAccount bankAccount = optionalBankAccount.get();
        Operation operation = createAndPerformOperation(bankAccount, amount, OperationType.WITHDRAWAL);
        operationRepository.save(operation);

        List<Operation> operationList = operationRepository.findByRefBankAccountId(bankAccount.getId());
        return dtoMapper.mapEntityToDto(bankAccount, operationList);
    }

    public AccountDto doDeposit(Long bankAccountNumber, long amount) throws NoSuchAccountException {
        Optional<BankAccount> optionalBankAccount = bankAccountRepository.findByBankAccountNumber(bankAccountNumber);
        if (!optionalBankAccount.isPresent()) {
            throw new NoSuchAccountException(bankAccountNumber);
        }
        BankAccount bankAccount = optionalBankAccount.get();

        Operation operation = createAndPerformOperation(bankAccount, amount, OperationType.DEPOSIT);
        operationRepository.save(operation);
        List<Operation> operationList = operationRepository.findByRefBankAccountId(bankAccount.getId());

        return dtoMapper.mapEntityToDto(bankAccount, operationList);
    }

    @VisibleForTesting
    Operation createAndPerformOperation(BankAccount bankAccount, long amount, OperationType operationType) throws NoSuchAccountException {

        Operation operation = new Operation();
        Long value = operationType.getValue(amount);
        operation.setAmount(value);
        operation.setDate(Instant.now());
        operation.setRefBankAccountId(bankAccount.getId());
        operation.setType(operationType);
        operation.setRefBankAccountId(bankAccount.getId());
        bankAccount.balance += value;
        operationRepository.save(operation);
        return operation;
    }
}

package fr.pyn.kata.bank.service;


import fr.pyn.kata.bank.domain.BankAccount;
import fr.pyn.kata.bank.domain.Operation;
import fr.pyn.kata.bank.domain.dto.AccountDto;
import fr.pyn.kata.bank.repository.BankAccountRepository;
import fr.pyn.kata.bank.repository.OperationRepository;
import fr.pyn.kata.bank.utils.NoSuchAccountException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BankAccountService {

    private final BankAccountRepository bankAccountRepository;
    private final OperationRepository operationRepository;
    private final AccountDtoMapper accountDtoMapper;

    public BankAccountService(BankAccountRepository bankAccountRepository, OperationRepository operationRepository, AccountDtoMapper accountDtoMapper) {
        this.bankAccountRepository = bankAccountRepository;
        this.operationRepository = operationRepository;
        this.accountDtoMapper = accountDtoMapper;
    }

    public List<Operation> listAllOperations(Long bankAccountNumber) throws NoSuchAccountException {
        Optional<BankAccount> optionalBankAccount = bankAccountRepository.findByBankAccountNumber(bankAccountNumber);
        if (!optionalBankAccount.isPresent()) {
            throw new NoSuchAccountException(bankAccountNumber);
        }

        return operationRepository.findByRefBankAccountId(optionalBankAccount.get().getId());
    }

    public AccountDto printStatement(Long bankAccountNumber) throws NoSuchAccountException {
        Optional<BankAccount> optionalBankAccount = bankAccountRepository.findByBankAccountNumber(bankAccountNumber);
        if (!optionalBankAccount.isPresent()) {
            throw new NoSuchAccountException(bankAccountNumber);
        }
        BankAccount bankAccount = optionalBankAccount.get();
        List<Operation> operationList = operationRepository.findByRefBankAccountId(bankAccount.getId());
        return accountDtoMapper.mapEntityToDto(bankAccount, operationList);
    }
}

package fr.pyn.kata.bank;

import fr.pyn.kata.bank.domain.BankAccount;
import fr.pyn.kata.bank.repository.BankAccountRepository;
import fr.pyn.kata.bank.repository.OperationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class BankAccountApp {

    @Autowired
    private BankAccountRepository bankAccountRepository;

    public static void main(String[] args) {
        SpringApplication.run(BankAccountApp.class, args);
    }

    @Bean
    public void initMongodb() {
        bankAccountRepository.deleteAll();

        BankAccount bankAccount1 = new BankAccount();
        bankAccount1.setBankAccountNumber(1L);
        bankAccount1.setBalance(7654321L);
        bankAccountRepository.save(bankAccount1);

        BankAccount bankAccount2 = new BankAccount();
        bankAccount2.setBankAccountNumber(2L);
        bankAccount2.setBalance(87654321);
        bankAccountRepository.save(bankAccount2);

        BankAccount bankAccount3 = new BankAccount();
        bankAccount3.setBankAccountNumber(3L);
        bankAccount3.setBalance(4321);
        bankAccountRepository.save(bankAccount3);

        BankAccount bankAccount4 = new BankAccount();
        bankAccount4.setBankAccountNumber(4L);
        bankAccount4.setBalance(0);
        bankAccountRepository.save(bankAccount4);
    }
}

package fr.pyn.kata.bank.repository;

import fr.pyn.kata.bank.domain.Operation;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface OperationRepository extends PagingAndSortingRepository<Operation, String> {

    List<Operation> findByRefBankAccountId(String refBankAccountId);
}
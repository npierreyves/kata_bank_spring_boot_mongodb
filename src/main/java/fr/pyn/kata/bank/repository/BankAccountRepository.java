package fr.pyn.kata.bank.repository;

import fr.pyn.kata.bank.domain.BankAccount;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface BankAccountRepository extends MongoRepository<BankAccount, String> {

    Optional<BankAccount> findByBankAccountNumber(Long bankAccountNumber);
}

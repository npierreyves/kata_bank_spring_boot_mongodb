package fr.pyn.kata.bank.utils;

public class NoSuchAccountException extends Exception {
    private final Long bankAccountNumber;

    public NoSuchAccountException(Long bankAccountNumber) {
        super("No account matched provided bank account number : " + bankAccountNumber);
        this.bankAccountNumber = bankAccountNumber;
    }

    public Long getBankAccountNumber() {
        return bankAccountNumber;
    }
}

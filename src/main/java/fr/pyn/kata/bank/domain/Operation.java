package fr.pyn.kata.bank.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.Instant;

@Document("operation")
public class Operation {

    @Id
    private String id;

    private Instant date;

    private OperationType type;

    private Long amount;

    @Indexed
    private String refBankAccountId;

    public Operation() {
    }

    public Operation(Instant date, OperationType type, long amount, String refBankAccountId) {
        this.date = date;
        this.type = type;
        this.amount = amount;
        this.refBankAccountId = refBankAccountId;
    }

    public String getRefBankAccountId() {
        return refBankAccountId;
    }

    public void setRefBankAccountId(String refBankAccountId) {
        this.refBankAccountId = refBankAccountId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Instant getDate() {
        return date;
    }

    public void setDate(Instant date) {
        this.date = date;
    }

    public OperationType getType() {
        return type;
    }

    public void setType(OperationType type) {
        this.type = type;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }
}

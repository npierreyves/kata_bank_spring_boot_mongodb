package fr.pyn.kata.bank.domain;

public enum OperationType {

    DEPOSIT("deposit", 1),
    WITHDRAWAL("withdrawal", -1);

    private String operation;
    private int coefficient;

    OperationType(String operation, int coefficient) {
        this.operation = operation;
        this.coefficient = coefficient;
    }

    public Long getValue(Long value) {
        return value * coefficient;
    }
}
